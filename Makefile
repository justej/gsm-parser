ifeq ($(OS),Windows_NT)
	ifeq ($(shell uname -s),) # not in a bash-like shell
		CLEANUP   = del /F /Q
		MKDIR     = mkdir
	else # in a bash-like shell, like msys
		CLEANUP   = rm -f
		MKDIR     = mkdir -p
	endif
	STD =
	TARGET_EXTENSION=exe
else
	CLEANUP = rm -f
	MKDIR   = mkdir -p
	STD     = -std=c99
	TARGET_EXTENSION=out
endif

.PHONY: clean
.PHONY: test
.PHONY: all

PATHU = test/Unity/src
PATHS = parser
PATHT = test
PATHB = build
PATHD = $(PATHB)/deps
PATHO = $(PATHB)/objs
PATHR = $(PATHB)/results

BUILD_PATHS = $(PATHB) $(PATHD) $(PATHO) $(PATHR)

COMPILE = gcc -c
LINK    = gcc
DEPEND  = gcc -MM -MG -MF
OPT     = -O0
CFLAGS  = -g $(OPT) $(STD) -DTEST -D__TEST_PRIVATE_FUNCTIONS__ -D__LOG_DEBUG__ -D__GSM_ALLOW_MULTI_IP_CONNECTION__ -D__GSM_HAL_DEBUG__

SRCC    = $(wildcard $(PATHS)/*.c)
SRCU    = $(wildcard $(PATHU)/*.c)
SRCT    = $(wildcard $(PATHT)/Test*.c)
ALLSRC  = $(SRCC) $(SRCU) $(SRCT)

INCC    = $(wildcard $(PATHS)/*.h)
INCU    = $(wildcard $(PATHU)/*.h)
ALLINC  = $(INCC) $(INCU)

OBJC    = $(addprefix $(PATHO)/,$(notdir $(SRCC:.c=.o)))
OBJU    = $(addprefix $(PATHO)/,$(notdir $(SRCU:.c=.o)))
OBJT    = $(addprefix $(PATHO)/,$(notdir $(SRCT:.c=.o)))
ALLOBJ  = $(OBJC) $(OBJU) $(OBJT)

EXECT   = $(addprefix $(PATHB)/,$(notdir $(SRCT:.c=.$(TARGET_EXTENSION))))

RESULTS = $(addprefix $(PATHR)/,$(notdir $(SRCT:.c=.txt)))

INC     = $(addprefix -I,$(PATHS) $(PATHU) $(PATHT))

PASSED  = `grep -s PASS $(PATHR)/*.txt | sed -E "s/.*?.txt://"`
FAIL    = `grep -s FAIL $(PATHR)/*.txt | sed -E "s/.*?.txt://"`
IGNORE  = `grep -s IGNORE $(PATHR)/*.txt | sed -E "s/.*?.txt://"`

show:
	@echo "ALLINC: $(ALLINC)"
	@echo "OBJC: $(OBJC)"
	@echo "OBJU: $(OBJU)"
	@echo "OBJT: $(OBJT)"

all: $(BUILD_PATHS) $(EXECT)

test: $(BUILD_PATHS) $(RESULTS)
	@echo "-----------------------"
	@echo "PASSED:"
	@echo "-----------------------"
	@echo "$(PASSED)"
	@echo "-----------------------"
	@echo "IGNORES:"
	@echo "-----------------------"
	@echo "$(IGNORE)"
	@echo "-----------------------"
	@echo "FAILURES:"
	@echo "-----------------------"
	@echo "$(FAIL)"
	@echo ""
	@echo "DONE"

$(RESULTS): $(PATHR)/%.txt: $(EXECT)
	-$(PATHB)/$*.$(TARGET_EXTENSION) > $(PATHR)/$*.txt 2>&1

$(EXECT): $(PATHB)/%.$(TARGET_EXTENSION): $(OBJT) $(OBJC) $(OBJU)
	$(LINK) $(OBJC) $(OBJU) $(PATHO)/$*.o -o $(PATHB)/$*.$(TARGET_EXTENSION)

$(OBJC): $(PATHO)/%.o: $(PATHS)/%.c $(ALLINC)
	$(COMPILE) $(CFLAGS) $(INC) $(PATHS)/$*.c -o $(PATHO)/$*.o

$(OBJU): $(PATHO)/%.o: $(PATHU)/%.c $(ALLINC)
	$(COMPILE) $(CFLAGS) $(INC) $(PATHU)/$*.c -o $(PATHO)/$*.o

$(OBJT): $(PATHO)/%.o: $(PATHT)/%.c $(OBJC) $(OBJU) $(ALLINC)
	$(COMPILE) $(CFLAGS) $(INC) $(PATHT)/$*.c -o $(PATHO)/$*.o

$(PATHB):
	$(MKDIR) $(PATHB)

$(PATHD):
	$(MKDIR) $(PATHD)

$(PATHO):
	$(MKDIR) $(PATHO)

$(PATHR):
	$(MKDIR) $(PATHR)

clean:
	$(CLEANUP) $(PATHS)/*.h.gch
	$(CLEANUP) -r $(PATHB)

.PRECIOUS: $(PATHB)/Test%.$(TARGET_EXTENSION)
.PRECIOUS: $(PATHB)/*
.PRECIOUS: $(PATHD)/%.d
.PRECIOUS: $(PATHO)/%.o
.PRECIOUS: $(PATHR)/%.txt

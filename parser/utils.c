#include "utils.h"


uint32_t time_get() {
  static uint32_t time = 0;
  return time++;
}

#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdint.h>
/* String *********************************************************************/

typedef struct {
  uint8_t len;
  uint8_t *data;
} String_t;

#define string_new(text) {sizeof(text) - 1, (uint8_t *) (text)}
#define string_empty(buffer) do { (ptr_pstr)->len = 0; (ptr_pstr)->data = (uint8_t *) (buffer); } while (0)
#define string_set(ptr_str, text) do { (ptr_str)->len = sizeof(text) - 1; (ptr_str)->data = (uint8_t *) text; } while (0)
#define string_setNull(str)  do {(str)->len = 0; (str)->data = NULL; } while(0)
#define string_charAt(str, index) (((index) > (str).len - 1) ? 0 : (str).data[(index)])

/* Logger *********************************************************************/

#ifdef __LOG_DEBUG__
#include <stdio.h>
#define logDebug(module, format, ...) fprintf(stderr, module ": " format, ##__VA_ARGS__)
#else
#define logDebug(module, format, ...) /* Nothing */
#endif /* __LOG_DEBUG__ */

/* Time ***********************************************************************/

uint32_t time_get();

#endif /* __UTILS_H__ */

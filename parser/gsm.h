#ifndef __GSM_PARSER_H__
#define __GSM_PARSER_H__

#include <stdbool.h>
#include "ring_buffer.h"
#include "utils.h"


/* GSM configuration start ****************************************************/

#define GSM_RX_BUFFER_COUNT 8U
#define GSM_RX_BUFFER_SIZE 128U


#define GSM_TX_BUFFER_COUNT 2U
#define GSM_TX_BUFFER_SIZE 556U

/* GSM configuration end ******************************************************/


/*
  List of feature switches:
  __GSM_ALLOW_MULTI_IP_CONNECTION__ - allow using multi-IP connect
*/

/* Data types *****************************************************************/

typedef struct _GSM GSM_t;


#include "gsm_hal.h" /* gsm.hal uses definition of GSM_t */


typedef enum {
  GSM_ATA,
  GSM_ATD,
  GSM_ATE,
  GSM_ATH,
  GSM_ATL,
  GSM_ATM,
  GSM_PLUS_PLUS_PLUS,
  GSM_ATO,
  GSM_AT_PLUS_GMI,
  GSM_AT_PLUS_GMM,
  GSM_AT_PLUS_GOI,
  GSM_AT_PLUS_GSN,
  GSM_AT_PLUS_IFC,
  GSM_AT_PLUS_IPR,
  GSM_AT_PLUS_HVOIC
} GSM_CommandName_t;


typedef enum {
  GSM_PIN_OK,
  GSM_PIN_NO,
} GSM_PinState_t;


typedef enum {
  GSM_NETWORK_NOT_REGISTERED,
  GSM_NETWORK_REGISTERED_HOME,
  GSM_NETWORK_SEARCHING,
  GSM_NETWORK_REGISTRATION_DENIED,
  GSM_NETWORK_UNKNOWN,
  GSM_NETWORK_REGISTERED_ROAMING,
} GSM_NetworkStatus_t;


typedef enum {
  GSM_MODE_IDLE, /* idle mode */
  GSM_MODE_CALL, /* active call mode */
  GSM_MODE_SMS,  /* active sms writing mode */
  GSM_MODE_HTTP, /* active data mode */
  GSM_MODE_FTP,  /* active data mode */
} GSM_Mode_t;


typedef enum {
  GSM_OK,
  GSM_BUSY,
  GSM_ERROR,
  GSM_TIMEOUT,
} GSM_Status_t;


typedef GSM_Status_t (*GSM_CommandResponseHandler_t)(GSM_t *gsm);
typedef GSM_Status_t (*GSM_UnsolicitedResponseHandler_t)(GSM_t *const gsm, String_t *response, uint8_t n);


typedef struct {
  GSM_Status_t status;
  String_t *details;
} GSM_Response_t;


typedef struct {
  String_t command;
  GSM_Mode_t state;
  GSM_CommandResponseHandler_t responseHandler;
} GSM_Command_t;


#ifdef __GSM_ALLOW_MULTI_IP_CONNECTION__
#define GSM_IP_CONNECIONS_MAX 8
#endif /* __GSM_ALLOW_MULTI_IP_CONNECTION__ */


struct _GSM {
  GSM_Command_t *activeCommand;
  String_t *activeCommandParameters;
  uint32_t activeCommandStartTime;
  uint32_t timeout;

  GSM_HAL_USART_t *usart;

  struct {
    GSM_PinState_t cpin;
    bool callReady;
    GSM_NetworkStatus_t networkRegistration;
#ifdef __GSM_ALLOW_MULTI_IP_CONNECTION__
    bool ipConnection[GSM_IP_CONNECIONS_MAX];
#else
    bool ipConnection;
#endif /* __GSM_ALLOW_MULTI_IP_CONNECTION__ */
    GSM_Mode_t mode;
  } state;

  RingBuffer_t rx;
  RingBuffer_t rxBufferPool[GSM_RX_BUFFER_COUNT];
  uint32_t rxComplete:1;
  uint32_t rxError:1;

  RingBuffer_t tx;
  RingBuffer_t txBufferPool[GSM_TX_BUFFER_COUNT];
  uint32_t txComplete:1;
  uint32_t txError:1;

  String_t *lastCommandDetails;
};

/* Prototypes *****************************************************************/

void GSM_initModule();

/**
 * @brief Initialize GSM module
 * @param gsm pointer to GSM definition
 * @param rxRingBuffer pointer to ring buffer of RX ring buffers
 * @param txRingBuffer pointer to ring buffer of TX ring buffers
 */
void GSM_init(GSM_t *gsm, RingBuffer_t *rxBackingRingBuffers, uint8_t *rxBackingBuffers, uint8_t rxRingBufferCount, uint16_t rxBackingBufferSize, 
                          RingBuffer_t *txBackingRingBuffers, uint8_t *txBackingBuffers, uint8_t txRingBufferCount, uint16_t txBackingBufferSize);

GSM_Status_t executeCommand(GSM_t *gsm, GSM_CommandName_t cmd, String_t *parameters);

GSM_Status_t parseResponse(GSM_t *gsm, String_t *cmd);

void irqHandler(GSM_t *gsm);

#endif /* __GSM_PARSER_H__ */

#ifndef __RING_BUFFER_H__
#define __RING_BUFFER_H__

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#ifndef __RING_BUFFER_DONT_USE_GENERIC__
#include "generic_ring_buffer.h"
#endif /* not __RING_BUFFER_DONT_USE_GENERIC__ */

#ifdef __RING_BUFFER_DEBUG__
#include <stdio.h>

#define printState(rb) printf("--% 3u: start: % 3lu;\tsize: % 3lu;\tcapacity: % 3lu;\tfirst byte: % 3u (%c)\tpre-last byte: % 3u (%c)\tlast byte: % 3u (%c)\n", \
        __LINE__,  (rb)->start, (rb)->size, (rb)->capacity, \
        (rb)->data[(rb)->start % (rb)->capacity], (rb)->data[(rb)->start % (rb)->capacity], \
        (rb)->data[((rb)->start + (rb)->size - 2) % (rb)->capacity], (rb)->data[((rb)->start + (rb)->size - 2) % (rb)->capacity], \
        (rb)->data[((rb)->start + (rb)->size - 1) % (rb)->capacity], (rb)->data[((rb)->start + (rb)->size - 1) % (rb)->capacity])

#define printBufferContent(rb) \
          for(uint8_t i = 0; i < rb.size; i++) { \
            uint8_t ch = rb.data[(rb.start + i) % rb.capacity]; \
            printf("  data[%u] = %x (%c)\n", i, ch, ch); \
          } \
          printf("\n");
#else
#define printState(rb) /* Nothing */
#define printBufferContent(rb)
#endif


/**
 * @brief Ring buffer data structure.
 * Features:
 * - allows storing data unless there's no capacity any more; in this case number
 *   of bytes to store doen't equal to number of actually stored bytes
 * - to facilicte debugging you may use directive __RING_BUFFER_DEBUG__.
 */
typedef struct _RingRuffer {
  uint32_t start;     /* Index of the head. Read from here */
  size_t   size;      /* Data length. Write to 'start + size' */
  size_t   capacity;  /* Buffer capacity */
  uint8_t *data;      /* Buffer data */
} RingBuffer_t;

/**
 * @brief Get number of used bytes
 * @param buffer_ptr pointer to the buffer
 * @return number of used bytes
 */
#define rb_getSize(buffer_ptr) ((buffer_ptr)->size)

/**
 * @brief Get total buffer size
 * @param buffer_ptr pointer to the buffer
 * @return total buffer size
 */
#define rb_getCapacity(buffer_ptr) ((buffer_ptr)->capacity)
/**
 * @brief Get total buffer size
 * @param buffer_ptr pointer to the buffer
 * @return total buffer size
 */
#define rb_isEmpty(buffer_ptr) ((buffer_ptr)->size == 0)
/**
 * @brief Get total buffer size
 * @param buffer_ptr pointer to the buffer
 * @return total buffer size
 */
#define rb_isFull(buffer_ptr) ((buffer_ptr)->size == (buffer_ptr)->capacity)

/**
 * @brief Create new ring buffer
 * @param buffer pointer to the backing buffer
 * @param capacity backing buffer size
 * @return ring buffer structure
 * @note Define directive __RING_BUFFER_DEBUG__ to force initializing the buffer with zeros
 */
RingBuffer_t rb_create(uint8_t *const buffer, const size_t capacity);

/**
 * @brief Put one byte to ring buffer
 * @param rbuffer pointer to the ring buffer
 * @param data byte to store
 * @return number of written bytes
 */
size_t rb_putOne(RingBuffer_t *const rbuffer, const uint8_t data);

/**
 * @brief Put multiple values to ring buffer
 * @param rbuffer pointer to the ring buffer
 * @param data array to store
 * @param size number of elements to store
 * @return number of written bytes
 */
size_t rb_putAll(RingBuffer_t *const rbuffer, const uint8_t *const data, const size_t size);

/**
 * @brief Get one byte from ring buffer
 * @param rbuffer pointer to the ring buffer
 * @param data address to store the return value to
 * @return number of read bytes
 */
size_t rb_getOne(RingBuffer_t *const rbuffer, uint8_t *const data);

/**
 * @brief Get multiple values from ring buffer
 * @param rbuffer pointer to the ring buffer
 * @param data array to store the result to
 * @param size
 * @return number of read bytes
 */
size_t rb_getAll(RingBuffer_t *const rbuffer, uint8_t *const data, const size_t size);

/**
 * @brief Test if element of a ring buffer equals to a given value
 * @param rbuffer pointer to the ring buffer
 * @param index index of the element to test
 * @param data givent value to compare to
 * @return true if element equals to the given value; false otherwise
 */
bool rb_testOne(const RingBuffer_t *const rbuffer, const size_t index, const uint8_t value);

/**
 * @brief Test if element of a ring buffer equals to a given value
 * @param rbuffer pointer to the ring buffer
 * @param rindex rindex of the element to test counting from the end of the buffer
 * @param data givent value to compare to
 * @return true if element equals to the given value; false otherwise
 */
#define rb_testOneLast(buffer_ptr, rindex, value) rb_testOne((buffer_ptr), (buffer_ptr)->size - (rindex) - 1, (value))

/**
 * @brief Reset buffer's internal state (effectively clear content of the buffer)
 * @param rbuffer ring buffer to reset
 */
void rb_reset(RingBuffer_t *const rbuffer);

/**
 * @brief Convert pointer to an array of bytes and to ring buffer
 * @param rbuffer pointer to the ring buffer
 * @param data byte to store
 * @return number of written bytes
 */
bool rb_putPointer(RingBuffer_t *rbuffer, const void *ptr);

/**
 * @brief Get array of bytes from ring buffer and convert to pointer
 * @param rbuffer pointer to the ring buffer
 * @return pointer from buffer or NULL if buffer is empty
 */
void rb_getPointer(RingBuffer_t *rbuffer, void **ptr);

/**
 * @brief Get array of bytes from ring buffer and convert to pointer. The value resides in the buffer
 * @param rbuffer pointer to the ring buffer
 * @return pointer from buffer or NULL if buffer is empty
 */
void rb_getLastPointer(RingBuffer_t *rbuffer, void **ptr);

#endif /* __RING_BUFFER_H__ */

#include "commons.h"
#include "gsm.h"
// #include "gsm_hal.h"

#ifdef __GSM_HAL_DEBUG__
static uint8_t usartByte;


void writeByteToUSART(uint8_t byte) {
  usartByte = byte;
}


uint8_t readByteFromUSART() {
  return usartByte;
}
#else
uint8_t readByteFromUSART() {
  /* Still doesn't do anything */
}
#endif /* __GSM_HAL_DEBUG__ */


bool GSM_HAL_send(GSM_t *gsm, String_t *text) {
#ifdef __GSM_HAL_DEBUG__
    logDebug("GSM HAL send", "%s", text->data);
    return true;
#else
    #error Not implemented. Yet
#endif /* __GSM_HAL_DEBUG__ */
}


bool GSM_HAL_receive(GSM_t *gsm) {
    gsm->rxComplete = false;
    gsm->rxError = false;
#ifdef __GSM_HAL_DEBUG__
    gsm->rxComplete = true;
    return true;
#else
    while (!gsm->rxComplete && !gsm->rxError) {
      /* wait */
    }
    return !gsm->rxError;
#endif /* __GSM_HAL_DEBUG__ */
}

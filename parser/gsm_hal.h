#ifndef __GSM_HAL_H__
#define __GSM_HAL_H__

#include <stdbool.h>
#include "utils.h"

#ifdef __GSM_HAL_DEBUG__
void writeByteToUSART(uint8_t byte);

uint8_t readByteFromUSART();
#else
/**
 * @brief Low-level implementation of data reading from USART
 */
uint8_t readByteFromUSART();
#endif /* __GSM_HAL_DEBUG__ */


typedef struct {} GSM_HAL_USART_t;


/**
 * @brief Send data to GSM module
 */
bool GSM_HAL_send(GSM_t *gsm, String_t *text);

/**
 * @brief Receive data from GSM module
 */
bool GSM_HAL_receive(GSM_t *gsm);

#endif /* __GSM_HAL_H__ */

#ifndef __GENERIC_RING_BUFFER_H__
#define __GENERIC_RING_BUFFER_H__

/**
 * @brief Get number of elements in the buffer
 * @param type type of elements
 * @param buffer_ptr pointer to the buffer
 * @return number of of elements
  */
#define grb_getSize(type, buffer_ptr) ((buffer_ptr)->size / sizeof(type))

/**
 * @brief Get buffer capacity
 * @param type type of elements
 * @param buffer_ptr pointer to the buffer
 * @return buffer capacity (number of elements)
 */
#define grb_getCapacity(type, buffer_ptr) ((buffer_ptr)->capacity / sizeof(type))
/**
 * @brief Check whether the buffer is empty
 * @param type type of elements
 * @param buffer_ptr pointer to the buffer
 * @return true if empty; false otherwise
 */
#define grb_isEmpty(type, buffer_ptr) ((buffer_ptr)->size == 0)
/**
 * @brief Check whether the buffer is full
 * @param type type of elements
 * @param buffer_ptr pointer to the buffer
 * @return true if full; false otherwise
 */
#define grb_isFull(type, buffer_ptr) ((buffer_prt)->size == (buffer_ptr)->capacity)

/**
 * @brief Create a new ring buffer
 * @param type type of elements
 * @param buffer pointer to the backing array
 * @param capacity capacity of the buffer (number of elements)
 * @return ring buffer
 * @note Define directive __RING_BUFFER_DEBUG__ to force initializing the buffer with zeros
 */
#define grb_create(type, buffer, capacity) rb_create((uint8_t *const)(buffer), (capacity) * sizeof(type))

/**
 * @brief Put one element to the ring buffer
 * @param type type of elements
 * @param rbuffer pointer to the ring buffer
 * @param data value to store
 * @return number of written elements
 *
 * IMPORTANT: you can't use grb_putOne() to put a constant or expression, i.e.
 * the following won't work:
 *   uint32_t buffer[10], array[2];
 *   RingBuffer_t rbuffer = grb_create(type, buffer, 10);
 *   size_t n1 = grb_putOne(uint32_t, rbuffer, (uint32_t)42); // won't work
 *   size_t n2 = grb_putOne(uint32_t, rbuffer, array + 1);    // Won't work
 *
 * Also, the grb_putOne() can't be used with with a static array since pointer
 * to a static array and pointer to the first element of a static array are
 * equal:
 *   uint8_t array[] = "123";
 *   uint8_t eq = array == &array; // equals 1, i.e. true
 */
#define grb_putOne(type, buffer, data) (rb_putAll((buffer), (const uint8_t *const )&(data), sizeof(type)) / sizeof(type))

/**
 * @brief Put multiple values to the ring buffer
 * @param type type of elements
 * @param rbuffer pointer to the ring buffer
 * @param data array of values to store
 * @param size number of elements to store
 * @return number of written elements
 */
#define grb_putAll(type, buffer, data, size) (rb_putAll((buffer), (const uint8_t *const)(data), size * sizeof(type)) / sizeof(type))

/**
 * @brief Get one element from the ring buffer
 * @param type type of elements
 * @param rbuffer pointer to the ring buffer
 * @param data address to store the result to
 * @return number of read elements
 */
#define grb_getOne(type, buffer, data) (rb_getAll((buffer), (uint8_t *const)(data), sizeof(type)) / sizeof(type))

/**
 * @brief Get multiple values from the ring buffer
 * @param type type of elements
 * @param rbuffer pointer to the ring buffer
 * @param data array to store the result to
 * @param size number of elements to read
 * @return number of read elements
 */
#define grb_getAll(type, buffer, data, size) (rb_getAll((buffer), (uint8_t *const)(data), size * sizeof(type)) / sizeof(type))

/**
 * @brief Reset the ring buffer
 * @param rbuffer ring buffer to reset
 */
#define grb_reset(buffer) rb_reset(rbuffer)

#endif /* __GENERIC_RING_BUFFER_H__ */

#include <stdint.h>

#ifdef __RING_BUFFER_DEBUG__
#include <stdio.h>
#endif /* __RING_BUFFER_DEBUG__ */

#include "ring_buffer.h"


#define SAFE_INDEX(start, offset, capacity) ((start + offset) % capacity)


RingBuffer_t rb_create(uint8_t *const buffer, const size_t capacity) {
  RingBuffer_t rbuffer = {0, 0, capacity, buffer};

#ifdef __RING_BUFFER_DEBUG__
  memset(buffer, 0, capacity);
#endif /* __RING_BUFFER_DEBUG__ */
  return rbuffer;
}


size_t rb_putOne(RingBuffer_t *const rbuffer, const uint8_t data) {
  if (rbuffer->size == rbuffer->capacity) {
    return 0;
  }

  rbuffer->data[SAFE_INDEX(rbuffer->start, (rbuffer->size)++, rbuffer->capacity)] = data;
  return 1;
}


size_t rb_putAll(RingBuffer_t *const rbuffer, uint8_t const *const data, const size_t size) {
  size_t n = (size < rbuffer->capacity - rbuffer->size) ? size : rbuffer->capacity - rbuffer->size;
  for (size_t i = 0; i < n; i++) {
    rbuffer->data[SAFE_INDEX(rbuffer->start, (rbuffer->size)++, rbuffer->capacity)] = data[i];
  }
  return n;
}


size_t rb_getOne(RingBuffer_t *const rbuffer, uint8_t *const data) {
  if (rbuffer->size == 0) {
    return 0;
  }

  *data = rbuffer->data[SAFE_INDEX((rbuffer->start)++, 0, rbuffer->capacity)];
  (rbuffer->size)--;
  return 1;
}


size_t rb_getAll(RingBuffer_t *const rbuffer, uint8_t *const data, const size_t size) {
  size_t n = (size < rbuffer->size) ? size : rbuffer->size;
  for (size_t i = 0; i < n; i++) {
    data[i] = rbuffer->data[SAFE_INDEX((rbuffer->start)++, 0, rbuffer->capacity)];
    (rbuffer->size)--;
  }
  return n;
}


bool rb_testOne(const RingBuffer_t *const rbuffer, const size_t index, const uint8_t value) {
  return index < rbuffer->size && rbuffer->data[SAFE_INDEX(rbuffer->start, 0, rbuffer->capacity)] == value;
}


void rb_reset(RingBuffer_t *const rbuffer) {
  rbuffer->start = rbuffer->size = 0;
}


bool rb_putPointer(RingBuffer_t *rbuffer, const void *ptr) {
  if (rbuffer->size == rbuffer->capacity) {
    return false;
  }

  uint8_t *p = (uint8_t *)&ptr;
  for (size_t i = 0; i < sizeof(ptr); i++) {
    rbuffer->data[SAFE_INDEX(rbuffer->start, (rbuffer->size)++, rbuffer->capacity)] = *(p + i);
  }

  return true;
}


void rb_getPointer(RingBuffer_t *rbuffer, void **ptr) {
  size_t n = (rbuffer->size < sizeof(void *)) ? rbuffer->size : sizeof(void *);

  if (n == 0) {
    *ptr = NULL;
    return;
  }

  uint8_t *p = (uint8_t *)ptr;
  for (size_t i = 0; i < n; i++) {
    *(p + i) = rbuffer->data[SAFE_INDEX(rbuffer->start++, 0, rbuffer->capacity)];
  }
  rbuffer->size -= n;
}


void rb_getLastPointer(RingBuffer_t *rbuffer, void **ptr) {
  size_t n = (rbuffer->size < sizeof(void *)) ? rbuffer->size : sizeof(void *);

  if (n == 0) {
    *ptr = NULL;
    return;
  }

  uint8_t *p = (uint8_t *)ptr;
  for (size_t i = 0; i < n; i++) {
    *(p + i) = rbuffer->data[SAFE_INDEX(rbuffer->start, rbuffer->size - n + i, rbuffer->capacity)];
  }
}

#include <stdbool.h>
#include "commons.h"
#include "gsm.h"
#include "utils.h"

#if __TEST_PRIVATE_FUNCTIONS__
#define Private
#else
#define Private static
#endif /* __TEST_PRIVATE_FUNCTIONS__ */


static void GSM_resetState(GSM_t *gsm);


GSM_Status_t GSM_defaultCommandResponseHandler(GSM_t *const gsm) {
  /* Do nothing */
}


static GSM_Command_t GSM_COMMANDS[] = {
  {string_new("ATA"), GSM_MODE_IDLE, GSM_defaultCommandResponseHandler},
  {string_new("ATD"), GSM_MODE_IDLE, GSM_defaultCommandResponseHandler},
  {string_new("ATE"), GSM_MODE_IDLE, GSM_defaultCommandResponseHandler},
  {string_new("ATH"), GSM_MODE_IDLE, GSM_defaultCommandResponseHandler},
  {string_new("ATL"), GSM_MODE_IDLE, GSM_defaultCommandResponseHandler},
  {string_new("ATM"), GSM_MODE_IDLE, GSM_defaultCommandResponseHandler},
  {string_new("PLUS_PLUS_PLUS"), GSM_MODE_IDLE, GSM_defaultCommandResponseHandler}
};


typedef struct {
  String_t code;
  GSM_UnsolicitedResponseHandler_t handler;
} GSM_UnsolicitedResponse_t;


/* Unsolicited code results start *********************************************/

Private GSM_Status_t urcHandlerDefault(GSM_t *const gsm, String_t *const response, const uint8_t n) {
  /* Do nothing */
  return GSM_OK;
}


Private GSM_Status_t urcHandlerCallReady(GSM_t *const gsm, String_t *const response, const uint8_t n) {
  gsm->state.callReady = true;
  return GSM_OK;
}


Private GSM_Status_t urcHandlerNetworkRegistration(GSM_t *const gsm, String_t *const response, const uint8_t n) {
  if (response->len > 0) {
    gsm->state.networkRegistration = (GSM_NetworkStatus_t) response->data;
  }
  return GSM_OK;
  /* TODO: Decide about using lac and ci */
}


Private GSM_Status_t urcHandlerSetIpConnection(GSM_t *const gsm, String_t *const response, const uint8_t n) {
#ifdef __GSM_ALLOW_MULTI_IP_CONNECTION__
  gsm->state.ipConnection[n - '0'] = true;
#else
  gsm->state.ipConnection = true;
#endif /* __GSM_ALLOW_MULTI_IP_CONNECTION__ */
  return GSM_OK;
}


Private GSM_Status_t urcHandlerUnsetIpConnection(GSM_t *const gsm, String_t *const response, const uint8_t n) {
#ifdef __GSM_ALLOW_MULTI_IP_CONNECTION__
  gsm->state.ipConnection[n - '0'] = false;
#else
  gsm->state.ipConnection = false;
#endif /* __GSM_ALLOW_MULTI_IP_CONNECTION__ */
  return GSM_OK;
}

/* Unsolicited code results end ***********************************************/


Private GSM_UnsolicitedResponse_t GSM_UNSOLICITED_RESPONSES[] = {
  {string_new("*PSNWID"), urcHandlerDefault},
  {string_new("*PSUTTZ:"), urcHandlerDefault},
  {string_new("+CALV:"), urcHandlerDefault},
  {string_new("+CBM:"), urcHandlerDefault},
  {string_new("+CCWA:"), urcHandlerDefault},
  {string_new("+CCWV"), urcHandlerDefault},
  {string_new("+CDNSGIP:"), urcHandlerDefault},
  {string_new("+CDRIND:"), urcHandlerDefault},
  {string_new("+CDS:"), urcHandlerDefault},
  {string_new("+CENG:"), urcHandlerDefault},
  {string_new("+CEXTBUT:"), urcHandlerDefault},
  {string_new("+CEXTHS:"), urcHandlerDefault},
  {string_new("+CFUN:"), urcHandlerDefault},
  {string_new("+CGURC:"), urcHandlerDefault},
  {string_new("+CHF:"), urcHandlerDefault},
  {string_new("+CLCC:"), urcHandlerDefault},
  {string_new("+CLIP:"), urcHandlerDefault},
  {string_new("+CMT:"), urcHandlerDefault},
  {string_new("+CMTE:"), urcHandlerDefault},
  {string_new("+CMTI:"), urcHandlerDefault},
  {string_new("+COLP:"), urcHandlerDefault},
  {string_new("+CPIN:"), urcHandlerDefault},
  {string_new("+CR:"), urcHandlerDefault},
  {string_new("+CREG:"), urcHandlerNetworkRegistration},
  {string_new("+CRING:"), urcHandlerDefault},
  {string_new("+CSMINS:"), urcHandlerDefault},
  {string_new("+CSQN:"), urcHandlerDefault},
  {string_new("+CSSI:"), urcHandlerDefault},
  {string_new("+CSSU:"), urcHandlerDefault},
  {string_new("+CTZV:"), urcHandlerDefault},
  {string_new("+CUSD:"), urcHandlerDefault},
  {string_new("+FTPDELE:"), urcHandlerDefault},
  {string_new("+FTPGET:"), urcHandlerDefault},
  {string_new("+FTPLIST:"), urcHandlerDefault},
  {string_new("+FTPMKD:"), urcHandlerDefault},
  {string_new("+FTPPUT:"), urcHandlerDefault},
  {string_new("+FTPRMD:"), urcHandlerDefault},
  {string_new("+FTPSIZE:"), urcHandlerDefault},
  {string_new("+HTTPACTION:"), urcHandlerDefault},
  {string_new("+IDP,"), urcHandlerDefault},
  {string_new("+PDP DEACT"), urcHandlerDefault},
  {string_new("+RECEIVE,"), urcHandlerDefault},
  {string_new("+SAPBR "), urcHandlerDefault}, // "+SAPBR <cid>:"
  {string_new("+SIMTONE:"), urcHandlerDefault},
  {string_new("+SKPD:"), urcHandlerDefault},
  {string_new("+STTONE:"), urcHandlerDefault},
  {string_new("ALREADY CONNECT"), urcHandlerDefault}, // "[<n>,]ALREADY CONNECT"
  {string_new("CHARGE-ONLY MODE"), urcHandlerDefault},
  {string_new("CLOSED"), urcHandlerUnsetIpConnection}, // "[<n>,]CLOSED"
  {string_new("CONNECT"), urcHandlerDefault},
  {string_new("CONNECT FAIL"), urcHandlerUnsetIpConnection}, // "[<n>,]CONNECT FAIL"
  {string_new("CONNECT OK"), urcHandlerSetIpConnection}, // "[<n>,]CONNECT OK"
  {string_new("Call Ready"), urcHandlerCallReady},
  {string_new("DST:"), urcHandlerDefault},
  {string_new("MO CONNECTED"), urcHandlerDefault},
  {string_new("MO RING"), urcHandlerDefault},
  {string_new("NORMAL POWER DOWN"), urcHandlerDefault},
  {string_new("RDY"), urcHandlerDefault},
  {string_new("RECV FROM:"), urcHandlerDefault},
  {string_new("REMOTE IP:"), urcHandlerDefault},
  {string_new("RING"), urcHandlerDefault},
  {string_new("SEND OK"), urcHandlerDefault}, // "[<n>,]SEND OK"
  {string_new("UNDER-VOLTAGE POWER DOWN"), urcHandlerDefault},
  {string_new("UNDER-VOLTAGE WARNING"), urcHandlerDefault}
};

/* Fast find string start *****************************************************/

#define NOT_FOUND UINT16_MAX
typedef uint16_t Index_t;


Private Index_t find(uint8_t symbol, Index_t lowerBoundary, Index_t upperBoundary, Index_t symbolPosition) {
  Index_t m;
  while (42)  {
    m = (lowerBoundary + upperBoundary) / 2;

    uint8_t c = string_charAt(GSM_UNSOLICITED_RESPONSES[m].code, symbolPosition);

    if (symbol == c) {
      return m;
    }

    if (lowerBoundary == upperBoundary) {
      return NOT_FOUND;
    }

    if (symbol < c) {
      upperBoundary = m;
    } else {
      lowerBoundary = m + 1;
    }
  }
}


Private Index_t findBoundary(uint8_t symbol, Index_t lowerBoundary, Index_t upperBoundary, Index_t symbolPosition, bool isUpperBoundary) {
  Index_t m = lowerBoundary; /* In case lowerBoundary == uppperBoundary */
  while (upperBoundary - lowerBoundary > 1) {
    m = (lowerBoundary + upperBoundary) / 2;

    uint8_t c = string_charAt(GSM_UNSOLICITED_RESPONSES[m].code, symbolPosition);

    if ((c == symbol) ^ isUpperBoundary) {
      upperBoundary = m;
    } else {
      lowerBoundary = m;
    }
  }

  if (isUpperBoundary) {
    return string_charAt(GSM_UNSOLICITED_RESPONSES[upperBoundary].code, symbolPosition) == symbol ? upperBoundary : lowerBoundary;
  } else {
    return string_charAt(GSM_UNSOLICITED_RESPONSES[lowerBoundary].code, symbolPosition) == symbol ? lowerBoundary : upperBoundary;
  }
}

/* Fast find string end *******************************************************/

#define isDigit(n) ((n) >= '0' && (n) <= '9')


Private GSM_Status_t parseUnsolicitedResponse(GSM_t *const gsm, String_t *const response) {
  uint8_t i = 0;
  uint8_t n = '\0';
#ifdef __GSM_ALLOW_MULTI_IP_CONNECTION__
  /* Some URCs have a leading number (e.g. [<n>],CONNECT OK), so look for it */
  if (response->len > 2 && isDigit(response->data[i])) {
    /* Read leading number if any */
    n = response->data[i++];
    /* Format is: number then comma */
    if (response->data[i++] != ',') {
      /* TODO: process bad response format */
      return GSM_ERROR;
    }
  }
#endif /* __GSM_ALLOW_MULTI_IP_CONNECTION__ */

  /* Find command */
  Index_t idx = NOT_FOUND;
  Index_t lowerBoundary = 0;
  Index_t upperBoundary = sizeof(GSM_UNSOLICITED_RESPONSES) / sizeof(GSM_UNSOLICITED_RESPONSES[0]) - 1;
  uint8_t j = 0; /* Indexes of 'response' and 'unsolicited result' are different */
  while(i < response->len) {
    if (lowerBoundary == upperBoundary) {
      /* Make sure the command matches till the end */
      if (j == GSM_UNSOLICITED_RESPONSES[idx].code.len) {
        break;
      }

      if (response->data[i] != string_charAt(GSM_UNSOLICITED_RESPONSES[idx].code, j)) {
        /* TODO: process bad command */
        return GSM_ERROR;
      }
    } else {
      idx = find(response->data[i], lowerBoundary, upperBoundary, j);
      if (idx == NOT_FOUND) {
        break;
      }

      lowerBoundary = findBoundary(response->data[i], lowerBoundary, idx, j, false);
      upperBoundary = findBoundary(response->data[i], idx, upperBoundary, j, true);
    }

    i++;
    j++;
  }

  if (idx == NOT_FOUND) {
    /* Command not found */
    return GSM_ERROR;
  }
  /* Handle response */
  String_t responseDetails = {response->len - i, &(response->data[i])};
  return GSM_UNSOLICITED_RESPONSES[idx].handler(gsm, &responseDetails, n);
}


#ifdef __TEST_PRIVATE_FUNCTIONS__
/* Utilities for testing start ************************************************/

Private GSM_Command_t *GSM_command(Index_t idx) {
  return &GSM_COMMANDS[idx];
}


Private String_t GSM_unsolicitedResponse(Index_t idx) {
  return GSM_UNSOLICITED_RESPONSES[idx].code;
}


Private size_t GSM_unsolicitedResponsesCount() {
  return sizeof(GSM_UNSOLICITED_RESPONSES) / sizeof(GSM_UNSOLICITED_RESPONSES[0]);
}


/* Utilities for testing end **************************************************/
#endif /* __TEST_PRIVATE_FUNCTIONS__ */


void GSM_init(GSM_t *gsm, RingBuffer_t *rxBackingRingBuffers, uint8_t *rxBackingBuffers, uint8_t rxRingBufferCount, uint16_t rxBackingBufferSize, 
                          RingBuffer_t *txBackingRingBuffers, uint8_t *txBackingBuffers, uint8_t txRingBufferCount, uint16_t txBackingBufferSize) {
  RingBuffer_t gsmRxBuffer = grb_create(RingBuffer_t *, rxBackingRingBuffers, rxRingBufferCount);
  rb_putPointer(&gsmRxBuffer, &rxBackingRingBuffers[0]);
  for (uint8_t i = 0; i < rxRingBufferCount; i++) {
    rxBackingRingBuffers[i] = rb_create(&rxBackingBuffers[i * rxBackingBufferSize], rxBackingBufferSize);
  }

  RingBuffer_t gsmTxBuffer = grb_create(RingBuffer_t *, txBackingRingBuffers, txRingBufferCount);
  rb_putPointer(&gsmTxBuffer, &txBackingRingBuffers[0]);
  for (uint8_t i = 0; i < txRingBufferCount; i++) {
    txBackingRingBuffers[i] = rb_create(&txBackingBuffers[i * txBackingBufferSize], txBackingBufferSize);
  }

  gsm->rx = gsmRxBuffer;
  gsm->rxComplete = false;
  gsm->rxError = false;

  gsm->tx = gsmTxBuffer;
  gsm->txComplete = false;
  gsm->rxError = false;

  gsm->usart = NULL; /* TODO: initialize */
  gsm->activeCommand = NULL;

  gsm->state.cpin = GSM_PIN_NO;
  gsm->state.callReady = false;
  gsm->state.networkRegistration = GSM_NETWORK_NOT_REGISTERED;
#ifdef __GSM_ALLOW_MULTI_IP_CONNECTION__
  for (uint8_t i = 0; i < GSM_IP_CONNECIONS_MAX; i++) {
    gsm->state.ipConnection[i] = false;
  }
#else
  gsm->state.ipConnection = false;
#endif /* __GSM_ALLOW_MULTI_IP_CONNECTION__ */
  gsm->state.mode = GSM_MODE_IDLE;
}


/**
 * @brief Send GSM command, receive and handle response
 * @param gsm ponter to the GSM
 * @param cmd command to execute
 * @param parameters command parameters (as a string)
 * @return number of used bytes
 */
GSM_Status_t executeCommand(GSM_t *gsm, GSM_CommandName_t cmd, String_t *parameters) {
  if (gsm->activeCommand != NULL) {
    return GSM_BUSY;
  }

  gsm->activeCommand = &GSM_COMMANDS[cmd];

  String_t at = string_new("AT");
  String_t crlf = string_new(CRLF);

  if (!GSM_HAL_send(gsm, &at)) {
    GSM_resetState(gsm);
    return GSM_ERROR;
  }

  if (!GSM_HAL_send(gsm, &(gsm->activeCommand->command))) {
    GSM_resetState(gsm);
    return GSM_ERROR;
  }

  if (parameters != NULL && !GSM_HAL_send(gsm, parameters)) {
    GSM_resetState(gsm);
    return GSM_ERROR;
  }

  if (!GSM_HAL_send(gsm, &crlf)) {
    GSM_resetState(gsm);
    return GSM_ERROR;
  }

  if (!GSM_HAL_receive(gsm)) {
    GSM_resetState(gsm);
    return GSM_ERROR;
  }

  return parseResponse(gsm, &(gsm->activeCommand->command));
}


GSM_Status_t parseResponse(GSM_t *gsm, String_t *cmd) {
  RingBuffer_t *rxBuffer;
  rb_getPointer(gsm, &rxBuffer);
  if (rxBuffer == NULL) {
    logDebug("parseResponse", "There's no response");
    return GSM_ERROR;
  }

  // * Command prefix
  // * Command result
  // * OK/ERROR/etc.

  if (rb_testOne(rxBuffer, 0, CR) && rb_testOne(rxBuffer, 1, LF)) {
    /* CRLF OK/ERROR CRLF */
  }
  /* TODO */
  return GSM_ERROR;
}


static void GSM_resetState(GSM_t *gsm) {
  /* TODO */
}


void irqHandler(GSM_t *gsm) {
  /* TODO: generify GSM_t to USART_t or so to reuse for, say, GPS etc. */
  RingBuffer_t *rxBuffer;

  rb_getLastPointer(&gsm->rx, (void **)&rxBuffer);
  if (rxBuffer == NULL) {
    logDebug("IRQ handler", "RX buffer doesn't exist\n");
    gsm->rxError = true;
    return;
  }

  if (rb_isFull(rxBuffer)) {
    uint8_t nextBufferIndex = ((uint8_t)(gsm->rxBufferPool - rxBuffer) + 1) % GSM_RX_BUFFER_COUNT;
    bool successfully = rb_putPointer(&gsm->rx, (void *)&(gsm->rxBufferPool[nextBufferIndex]));
    if (!successfully) {
      logDebug("IRQ handler", "No free buffers\n");
      gsm->rxError = true;
      return;
    }
  }

  uint8_t byte = readByteFromUSART();
  uint32_t n = rb_putOne(rxBuffer, byte);
  if (n == 0) {
    /* TODO: handle error here */
    logDebug("IRQ handler", "Buffer overflow\n");
    gsm->rxError = true;
    return;
  }

  if ((gsm->state.mode > GSM_MODE_CALL) /* In data modes (SMS/HTTP/FTP etc.) pass data without pre-processing */
  || (rb_getSize(rxBuffer) > 2 && rb_testOneLast(rxBuffer, 0, LF))) { /* something + CRLF */
      gsm->rxComplete = true;
  }
}
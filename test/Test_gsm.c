#include <stdint.h>
#include <stddef.h>
#include <stdio.h>


#include "unity.h"
#include "commons.h"
#include "ring_buffer.h"
#include "gsm.h"
#include "utils.h"

/* Backing ring buffers */
static RingBuffer_t gsmRxBackingRingBuffers[GSM_RX_BUFFER_COUNT];
static RingBuffer_t gsmTxBackingRingBuffers[GSM_TX_BUFFER_COUNT];
/* Backing byte buffers */
static uint8_t gsmRxBackingBuffers[GSM_RX_BUFFER_COUNT * GSM_RX_BUFFER_SIZE];
static uint8_t gsmTxBackingBuffers[GSM_TX_BUFFER_COUNT * GSM_TX_BUFFER_SIZE];


#ifdef __TEST_PRIVATE_FUNCTIONS__
#define NOT_FOUND UINT16_MAX
typedef uint16_t Index_t;
Index_t find(uint8_t symbol, Index_t lowerBoundary, Index_t upperBoundary, Index_t symbolPosition);
Index_t findBoundary(uint8_t symbol, Index_t lowerBoundary, Index_t upperBoundary, Index_t symbolPosition, bool isUpperBoundary);


GSM_Command_t *GSM_command(Index_t idx);
String_t GSM_unsolicitedResponse(Index_t idx);
size_t GSM_unsolicitedResponsesCount();


#include <string.h>


static void test_find_sanity() {
  Index_t idx;
  Index_t lowerBoundary;
  Index_t upperBoundary;
  Index_t symbolIndex;

  /* Symbol index = 0 */
  lowerBoundary = 0;
  upperBoundary = 63;
  symbolIndex = 0;

  idx = find('D', lowerBoundary, upperBoundary, symbolIndex);
  TEST_ASSERT_EQUAL_INT16(53, idx);
  TEST_ASSERT_EQUAL_STRING("DST:", GSM_unsolicitedResponse(idx).data);

  idx = find('*', lowerBoundary, upperBoundary, symbolIndex);
  TEST_ASSERT_TRUE(idx == 0 || idx == 1);

  idx = find('U', lowerBoundary, upperBoundary, symbolIndex);
  TEST_ASSERT_TRUE(idx == GSM_unsolicitedResponsesCount() - 2 || idx == GSM_unsolicitedResponsesCount() - 1);

  idx = find('Z', lowerBoundary, upperBoundary, symbolIndex);
  TEST_ASSERT_TRUE(idx == NOT_FOUND);

  /* Symbol index = 2 */
  lowerBoundary = 2;
  upperBoundary = 2 + 29;
  symbolIndex = 2;

  idx = find('B', lowerBoundary, upperBoundary, symbolIndex);
  TEST_ASSERT_EQUAL_INT16(3, idx);
  TEST_ASSERT_EQUAL_STRING("+CBM:", GSM_unsolicitedResponse(idx).data);

    idx = find('D', lowerBoundary, upperBoundary, symbolIndex);
    TEST_ASSERT_TRUE(idx >=6 && idx <= 8);
}


static void test_find_corner_cases() {
  Index_t idx;
  Index_t lowerBoundary;
  Index_t upperBoundary;
  Index_t symbolIndex;

  /* Test lowerBoundary == upperBoundary*/
  lowerBoundary = 0;
  upperBoundary = 0;
  symbolIndex = 3;

  idx = find('N', lowerBoundary, upperBoundary, symbolIndex);
  TEST_ASSERT_TRUE(idx == 0);
  TEST_ASSERT_EQUAL_STRING("*PSNWID", GSM_unsolicitedResponse(idx).data);

  /* Test boundaries */
  lowerBoundary = 0;
  upperBoundary = 1;
  symbolIndex = 3;

  /* lowerBoundary */
  idx = find('N', lowerBoundary, upperBoundary, symbolIndex);
  TEST_ASSERT_TRUE(idx == 0);
  TEST_ASSERT_EQUAL_STRING("*PSNWID", GSM_unsolicitedResponse(idx).data);

  /* upperBoundary */
  idx = find('U', lowerBoundary, upperBoundary, symbolIndex);
  TEST_ASSERT_TRUE(idx == 1);
  TEST_ASSERT_EQUAL_STRING("*PSUTTZ:", GSM_unsolicitedResponse(idx).data);

  /* beyond lowerBoundary */
  idx = find('!', lowerBoundary, upperBoundary, symbolIndex);
  TEST_ASSERT_TRUE(idx == NOT_FOUND);

  /* beyond upperBoundary */
  lowerBoundary = 62;
  upperBoundary = 63;
  symbolIndex = 14;
  idx = find('Z', lowerBoundary, upperBoundary, symbolIndex);
  TEST_ASSERT_TRUE(idx == NOT_FOUND);

  /* Test index exceeds length of response code */
  lowerBoundary = 49;
  upperBoundary = 50;
  symbolIndex = 7;
  idx = find(' ', lowerBoundary, upperBoundary, symbolIndex);
  TEST_ASSERT_TRUE(idx == 50);
  TEST_ASSERT_EQUAL_STRING("CONNECT FAIL", GSM_unsolicitedResponse(idx).data);
}


static void test_findBoundary() {
  Index_t idx;
  Index_t lowerBoundary;
  Index_t upperBoundary;
  Index_t symbolIndex;

  lowerBoundary = 0;
  upperBoundary = 63;
  symbolIndex = 0;

  /* Sanity test */
  idx = findBoundary('+', lowerBoundary, upperBoundary, symbolIndex, false);
  TEST_ASSERT_EQUAL_INT16(2, idx);

  idx = findBoundary('+', lowerBoundary, upperBoundary, symbolIndex, true);
  TEST_ASSERT_EQUAL_INT16(45, idx);

  /* Test different offset */
  idx = findBoundary('+', lowerBoundary + 1, upperBoundary, symbolIndex, false);
  TEST_ASSERT_EQUAL_INT16(2, idx);

  idx = findBoundary('+', lowerBoundary, upperBoundary - 1, symbolIndex, true);
  TEST_ASSERT_EQUAL_INT16(45, idx);
}


GSM_Status_t parseUnsolicitedResponse(GSM_t *const gsm, String_t *const response);


static void test_parseUnsolicitedResponse_callReady() {
  GSM_t gsmTest;
  /* Precondition */
  gsmTest.state.callReady = false;

  TEST_ASSERT_FALSE(gsmTest.state.callReady);

  /* Test itself */
  String_t resp = string_new("Call Ready" CRLF);
  GSM_Status_t status = parseUnsolicitedResponse(&gsmTest, &resp);

  TEST_ASSERT_TRUE(GSM_OK == status);
  TEST_ASSERT_TRUE(gsmTest.state.callReady);
}


static void test_parseUnsolicitedResponse_connectOk() {
  GSM_t gsmTest;
#ifdef __GSM_ALLOW_MULTI_IP_CONNECTION__
  /* Precondition */
  gsmTest.state.ipConnection[0] = false;
  gsmTest.state.ipConnection[1] = false;
  gsmTest.state.ipConnection[2] = true;

  TEST_ASSERT_FALSE(gsmTest.state.ipConnection[0]);
  TEST_ASSERT_FALSE(gsmTest.state.ipConnection[1]);
  TEST_ASSERT_TRUE(gsmTest.state.ipConnection[2]);

  String_t resp = string_new("0,CONNECT OK" CRLF);

  /* Test itself */
  GSM_Status_t status = parseUnsolicitedResponse(&gsmTest, &resp);

  TEST_ASSERT_TRUE(GSM_OK == status);
  TEST_ASSERT_TRUE(gsmTest.state.ipConnection[0]);
  TEST_ASSERT_FALSE(gsmTest.state.ipConnection[1]);
  TEST_ASSERT_TRUE(gsmTest.state.ipConnection[2]);
#else
  /* Precondition */
  gsmTest.state.ipConnection = false;

  TEST_ASSERT_FALSE(gsmTest.state.ipConnection);

  String_t resp = string_new("CONNECT OK" CRLF);

  /* Test itself */
  GSM_Status_t status = parseUnsolicitedResponse(&gsmTest, &resp);

  TEST_ASSERT_TRUE(GSM_OK == status);
  TEST_ASSERT_TRUE(gsmTest.state.ipConnection);
#endif /* __GSM_ALLOW_MULTI_IP_CONNECTION__*/
}


static void test_parseUnsolicitedResponse_connectFail() {
  GSM_t gsmTest;
#ifdef __GSM_ALLOW_MULTI_IP_CONNECTION__
  /* Precondition */
  gsmTest.state.ipConnection[0] = true;
  gsmTest.state.ipConnection[1] = true;
  gsmTest.state.ipConnection[2] = false;

  TEST_ASSERT_TRUE(gsmTest.state.ipConnection[0]);
  TEST_ASSERT_TRUE(gsmTest.state.ipConnection[1]);
  TEST_ASSERT_FALSE(gsmTest.state.ipConnection[2]);

  String_t resp = string_new("0,CONNECT FAIL" CRLF);

  /* Test itself */
  GSM_Status_t status = parseUnsolicitedResponse(&gsmTest, &resp);

  TEST_ASSERT_TRUE(GSM_OK == status);
  TEST_ASSERT_FALSE(gsmTest.state.ipConnection[0]);
  TEST_ASSERT_TRUE(gsmTest.state.ipConnection[1]);
  TEST_ASSERT_FALSE(gsmTest.state.ipConnection[2]);
#else
  /* Precondition */
  gsmTest.state.ipConnection = true;

  TEST_ASSERT_TRUE(gsmTest.state.ipConnection);

  String_t resp = string_new("CONNECT FAIL" CRLF);

  /* Test itself */
  GSM_Status_t status = parseUnsolicitedResponse(&gsmTest, &resp);

  TEST_ASSERT_TRUE(GSM_OK == status);
  TEST_ASSERT_FALSE(gsmTest.state.ipConnection);
#endif /* __GSM_ALLOW_MULTI_IP_CONNECTION__*/
}


static void test_parseUnsolicitedResponse_closed() {
  GSM_t gsmTest;
#ifdef __GSM_ALLOW_MULTI_IP_CONNECTION__
  /* Precondition */
  gsmTest.state.ipConnection[0] = true;
  gsmTest.state.ipConnection[1] = true;
  gsmTest.state.ipConnection[2] = false;

  TEST_ASSERT_TRUE(gsmTest.state.ipConnection[0]);
  TEST_ASSERT_TRUE(gsmTest.state.ipConnection[1]);
  TEST_ASSERT_FALSE(gsmTest.state.ipConnection[2]);

  String_t resp = string_new("0,CLOSED" CRLF);

  /* Test itself */
  GSM_Status_t status = parseUnsolicitedResponse(&gsmTest, &resp);

  TEST_ASSERT_TRUE(GSM_OK == status);
  TEST_ASSERT_FALSE(gsmTest.state.ipConnection[0]);
  TEST_ASSERT_TRUE(gsmTest.state.ipConnection[1]);
  TEST_ASSERT_FALSE(gsmTest.state.ipConnection[2]);
#else
  /* Precondition */
  gsmTest.state.ipConnection = true;

  TEST_ASSERT_TRUE(gsmTest.state.ipConnection);

  String_t resp = string_new("CLOSED" CRLF);

  /* Test itself */
  GSM_Status_t status = parseUnsolicitedResponse(&gsmTest, &resp);

  TEST_ASSERT_TRUE(GSM_OK == status);
  TEST_ASSERT_FALSE(gsmTest.state.ipConnection);
#endif /* __GSM_ALLOW_MULTI_IP_CONNECTION__*/
}
#else
#error Private functions testing is disabled
#endif /* __TEST_PRIVATE_FUNCTIONS__ */


static void test_executeCommandDuringActiveCommand() {
  GSM_t gsm;

  GSM_init(&gsm, gsmRxBackingRingBuffers, gsmRxBackingBuffers, GSM_RX_BUFFER_COUNT, GSM_RX_BUFFER_SIZE,
                 gsmTxBackingRingBuffers, gsmRxBackingBuffers, GSM_TX_BUFFER_COUNT, GSM_TX_BUFFER_SIZE);

  gsm.activeCommand = GSM_command(GSM_ATD);

  GSM_Status_t status = executeCommand(&gsm, GSM_ATE, NULL);

  TEST_ASSERT_EQUAL_INT(GSM_BUSY, status);
}


static void receiveResponse(GSM_t *gsm, String_t *response) {
  for (uint32_t i = 0; i < response->len; i++) {
    writeByteToUSART(response->data[i]);
    irqHandler(gsm);
  }
}


static void test_originateCall() {
  String_t response = string_new(CRLF "OK" CRLF);

  GSM_t gsm;
  GSM_init(&gsm, gsmRxBackingRingBuffers, gsmRxBackingBuffers, GSM_RX_BUFFER_COUNT, GSM_RX_BUFFER_SIZE,
                 gsmTxBackingRingBuffers, gsmRxBackingBuffers, GSM_TX_BUFFER_COUNT, GSM_TX_BUFFER_SIZE);
  receiveResponse(&gsm, &response);

  GSM_Status_t status = executeCommand(&gsm, GSM_ATE, NULL);

  TEST_ASSERT_EQUAL(GSM_OK, status);
}

int main() {
  UNITY_BEGIN();

  RUN_TEST(test_find_sanity);
  RUN_TEST(test_find_corner_cases);
  RUN_TEST(test_findBoundary);

  RUN_TEST(test_parseUnsolicitedResponse_callReady);
  RUN_TEST(test_parseUnsolicitedResponse_connectOk);
  RUN_TEST(test_parseUnsolicitedResponse_connectFail);
  RUN_TEST(test_parseUnsolicitedResponse_closed);

  RUN_TEST(test_executeCommandDuringActiveCommand);
  RUN_TEST(test_originateCall);

  return UNITY_END();
}

#include <stdint.h>
#include <string.h>

#define __RING_BUFFER_DEBUG__

#include "unity.h"
#include "ring_buffer.h"

/******************************************************************************/
/* START OF TEST RING BUFFER **************************************************/
/******************************************************************************/

static void test_rb_create() {
  uint8_t buffer[42];
  size_t n = sizeof(buffer);
  RingBuffer_t ringBuffer = rb_create(buffer, n);

  TEST_ASSERT_EQUAL_UINT32(0, rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));
  TEST_ASSERT_TRUE(ringBuffer.data == buffer);

#ifdef __RING_BUFFER_DEBUG__
  uint8_t data;
  for (uint8_t i = 0; i < rb_getCapacity(&ringBuffer); i++) {
    rb_getOne(&ringBuffer, &data);

    TEST_ASSERT_EQUAL_UINT8(0, data);
  }
#endif
}


static void test_rb_putOne_rb_getOne() {
  uint8_t buffer[42];
  size_t n = sizeof(buffer);
  RingBuffer_t ringBuffer = rb_create(buffer, n);

  rb_putOne(&ringBuffer, 10);

  TEST_ASSERT_EQUAL_UINT32(1, rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  rb_putOne(&ringBuffer, 20);

  TEST_ASSERT_EQUAL_UINT32(2, rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  uint8_t data;
  rb_getOne(&ringBuffer, &data);

  TEST_ASSERT_EQUAL_UINT32(1, rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT8(10, data);

  rb_getOne(&ringBuffer, &data);

  TEST_ASSERT_EQUAL_UINT32(0, rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT8(20, data);
}


static void test_rb_putAll_normal() {
  uint8_t buffer[42];
  size_t n = sizeof(buffer);
  RingBuffer_t ringBuffer = rb_create(buffer, n);
  uint8_t tbuffer[] = "test data";
  size_t tn = sizeof(tbuffer);

  TEST_ASSERT_EQUAL_UINT32(tn, rb_putAll(&ringBuffer, tbuffer, tn));
  TEST_ASSERT_EQUAL_UINT32(tn, rb_putAll(&ringBuffer, tbuffer, tn));
  TEST_ASSERT_EQUAL_UINT32(2 * tn, rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  uint8_t data;
  for (uint8_t i = 0; i < tn; i++) {

    TEST_ASSERT_EQUAL_UINT32(1, rb_getOne(&ringBuffer, &data));
    TEST_ASSERT_EQUAL_UINT8(tbuffer[i], data);

  }

  for (uint8_t i = 0; i < tn; i++) {

    TEST_ASSERT_EQUAL_UINT32(1, rb_getOne(&ringBuffer, &data));
    TEST_ASSERT_EQUAL_UINT8(tbuffer[i], data);

  }
}


static void test_rb_putAll_overflow() {
  uint8_t buffer[4];
  size_t n = sizeof(buffer);
  RingBuffer_t ringBuffer = rb_create(buffer, n);
  uint8_t tbuffer[] = "test data";
  size_t tn = sizeof(tbuffer);

  TEST_ASSERT_EQUAL_UINT32(n, rb_putAll(&ringBuffer, tbuffer, tn));

  uint8_t data;
  for (uint8_t i = 0; i < n; i++) {

    TEST_ASSERT_EQUAL_UINT32(1, rb_getOne(&ringBuffer, &data));
    TEST_ASSERT_EQUAL_UINT8(tbuffer[i], data);

  }

  for (uint8_t i = n; i < tn; i++) {

    TEST_ASSERT_EQUAL_UINT32(0, rb_getOne(&ringBuffer, &data));

  }
}


static void test_rb_getAll() {
  uint8_t buffer[42];
  size_t n = sizeof(buffer);
  RingBuffer_t ringBuffer = rb_create(buffer, n);
  uint8_t tbuffer[] = "test data";
  size_t tn = sizeof(tbuffer);

  TEST_ASSERT_EQUAL_UINT32(tn, rb_putAll(&ringBuffer, tbuffer, tn));

  uint8_t data[42];

  TEST_ASSERT_EQUAL_UINT32(tn, rb_getAll(&ringBuffer, data, sizeof(data)));
  TEST_ASSERT_EQUAL_UINT8_ARRAY(tbuffer, data, tn);
}


static void test_rb_testOneLast_normal() {
  uint8_t buffer[42];
  size_t n = sizeof(buffer);
  RingBuffer_t ringBuffer = rb_create(buffer, n);
  uint8_t tbuffer[] = "test data";
  size_t tn = sizeof(tbuffer);

  for (uint8_t i = 0; i < tn; i++) {

    TEST_ASSERT_EQUAL_UINT32(rb_putOne(&ringBuffer, tbuffer[i]), 1);
    TEST_ASSERT_TRUE(rb_testOneLast(&ringBuffer, 0, tbuffer[i]));

  }

  for (uint8_t i = 0; i < tn; i++) {

    TEST_ASSERT_TRUE(rb_testOneLast(&ringBuffer, i, tbuffer[tn - i - 1]));

  }
}


static void test_rb_testOneLast_beyond_limits() {
  uint8_t buffer[42];
  size_t n = sizeof(buffer);
  RingBuffer_t ringBuffer = rb_create(buffer, n);
  uint8_t tbuffer[] = "test data";
  size_t tn = sizeof(tbuffer);

  rb_putAll(&ringBuffer, tbuffer + 1, 5);

  /* Two tests in a row to make sure the byte remains */
  TEST_ASSERT_TRUE(rb_testOneLast(&ringBuffer, 0, 'd'));
  TEST_ASSERT_TRUE(rb_testOneLast(&ringBuffer, 0, 'd'));
  TEST_ASSERT_TRUE(rb_testOneLast(&ringBuffer, 4, 'e'));
  TEST_ASSERT_FALSE(rb_testOneLast(&ringBuffer, 6, 't'));
  /* rb_testOneLast doesn't work with negative indices */
  TEST_ASSERT_FALSE(rb_testOneLast(&ringBuffer, -1, 'a'));
  TEST_ASSERT_FALSE(rb_testOneLast(&ringBuffer, -1, 'd'));
  TEST_ASSERT_FALSE(rb_testOneLast(&ringBuffer, -1, ' '));
}


static void test_rb_reset() {
  uint8_t buffer[42];
  size_t n = sizeof(buffer);
  RingBuffer_t ringBuffer = rb_create(buffer, n);
  uint8_t tbuffer[] = "test data";
  size_t tn = sizeof(tbuffer);

  rb_putAll(&ringBuffer, tbuffer, tn);
  rb_putAll(&ringBuffer, tbuffer, tn);
  /* Change start position */
  rb_getAll(&ringBuffer, tbuffer, tn);

  /* It's better to not consider as white box, I know */
  TEST_ASSERT_NOT_EQUAL(0, ringBuffer.start);
  TEST_ASSERT_EQUAL_UINT32(tn, rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  rb_reset(&ringBuffer);

  TEST_ASSERT_EQUAL_UINT32(0, ringBuffer.start);
  TEST_ASSERT_EQUAL_UINT32(0, rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));
}
/******************************************************************************/
/* END OF TEST GENERIC RING BUFFER ********************************************/
/******************************************************************************/

#ifndef __RING_BUFFER_DONT_USE_GENERIC__
/******************************************************************************/
/* START OF TEST GENERIC RING BUFFER ******************************************/
/******************************************************************************/

static void test_grb_create() {
  /* Ring buffer of pointers to uint8_t */
  uint8_t *buffer[42];
  size_t n = sizeof(buffer) / sizeof(buffer[0]);
  RingBuffer_t ringBuffer = grb_create(uint8_t *, buffer, n);

  TEST_ASSERT_EQUAL_UINT32(0, rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n * sizeof(uint8_t *), rb_getCapacity(&ringBuffer));
  TEST_ASSERT_TRUE(ringBuffer.data == (uint8_t *)buffer);

  /* Ring buffer of integers uint16_t */
  uint16_t buffer16[42];
  size_t n16 = sizeof(buffer16) / sizeof(buffer16[0]);
  RingBuffer_t ringBuffer16 = grb_create(uint16_t, buffer16, n16);

  TEST_ASSERT_EQUAL_UINT32(0, grb_getSize(uint16_t, &ringBuffer16));
  TEST_ASSERT_EQUAL_UINT32(n16 * sizeof(uint16_t), rb_getCapacity(&ringBuffer16));
  TEST_ASSERT_TRUE(ringBuffer16.data == (uint8_t *)buffer16);
}


static void test_grb_putOne_grb_getOne() {
  uint8_t *buffer[42];
  size_t n = sizeof(buffer) / sizeof(buffer[0]);
  RingBuffer_t ringBuffer = grb_create(uint8_t *, buffer, n);
  uint8_t one[] = "one", two[] = "two";
  uint8_t *t;

  /*
   * IMPORTANT: you can't use grb_putOne() to put a constant, i.e. the following
   * won't work:
   *   uint32_t buffer[10];
   *   RingBuffer_t rbuffer = grb_create(type, buffer, 10);
   *   size_t n = grb_putOne(uint32_t, rbuffer, (uint32_t)42);
   *
   * Also, the grb_putOne() can't be used with with a static array since pointer
   * to a static array and pointer to the first element of a static array are
   * equal:
   *   uint8_t array[] = "123";
   *   uint8_t eq = array == &array; // equals 1, i.e. true
   */

  t = one;
  grb_putOne(uint8_t *, &ringBuffer, t);

  TEST_ASSERT_EQUAL_UINT32(sizeof(buffer[0]), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n * sizeof(buffer[0]), rb_getCapacity(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(1, grb_getSize(uint8_t *, &ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, grb_getCapacity(uint8_t *, &ringBuffer));

  t = two;
  grb_putOne(uint8_t *, &ringBuffer, t);

  TEST_ASSERT_EQUAL_UINT32(2 * sizeof(buffer[0]), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n * sizeof(buffer[0]), rb_getCapacity(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(2, grb_getSize(uint8_t *, &ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, grb_getCapacity(uint8_t *, &ringBuffer));

  uint8_t *data;
  grb_getOne(uint8_t *, &ringBuffer, &data);

  TEST_ASSERT_EQUAL_UINT32(sizeof(buffer[0]), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n * sizeof(buffer[0]), rb_getCapacity(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(1, grb_getSize(uint8_t *, &ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, grb_getCapacity(uint8_t *, &ringBuffer));
  TEST_ASSERT_TRUE(one == data);

  grb_getOne(uint8_t *, &ringBuffer, &data);

  TEST_ASSERT_EQUAL_UINT32(0, rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n * sizeof(buffer[0]), rb_getCapacity(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(0, grb_getSize(uint8_t *, &ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, grb_getCapacity(uint8_t *, &ringBuffer));
  TEST_ASSERT_TRUE(two == data);
}


static void test_grb_putAll_normal() {
  uint8_t *buffer[42];
  size_t n = sizeof(buffer) / sizeof(buffer[0]);
  RingBuffer_t ringBuffer = grb_create(uint8_t *, buffer, n);
  uint8_t *tbuffer[3];
  size_t tn = sizeof(tbuffer) / sizeof(tbuffer[0]);
  tbuffer[0] = "one";
  tbuffer[1] = "two";
  tbuffer[2] = "three";

  TEST_ASSERT_EQUAL_UINT32(0, rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n * sizeof(uint8_t *), rb_getCapacity(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(0, grb_getSize(uint8_t *, &ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, grb_getCapacity(uint8_t *, &ringBuffer));

  /*
   * Write the array twice
  */
  size_t written;
  written = grb_putAll(uint8_t *, &ringBuffer, tbuffer, tn);

  TEST_ASSERT_EQUAL_UINT32(tn, written);
  TEST_ASSERT_EQUAL_UINT32(tn, grb_getSize(uint8_t *, &ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, grb_getCapacity(uint8_t *, &ringBuffer));

  written = grb_putAll(uint8_t *, &ringBuffer, tbuffer, tn);

  TEST_ASSERT_EQUAL_UINT32(tn, written);
  TEST_ASSERT_EQUAL_UINT32(2 * tn, grb_getSize(uint8_t *, &ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, grb_getCapacity(uint8_t *, &ringBuffer));

  uint8_t *data;

  TEST_ASSERT_EQUAL_UINT32(1, grb_getOne(uint8_t *, &ringBuffer, &data));
  TEST_ASSERT_TRUE(tbuffer[0] == data);
  TEST_ASSERT_EQUAL_UINT32(1, grb_getOne(uint8_t *, &ringBuffer, &data));
  TEST_ASSERT_TRUE(tbuffer[1] == data);
  TEST_ASSERT_EQUAL_UINT32(1, grb_getOne(uint8_t *, &ringBuffer, &data));
  TEST_ASSERT_TRUE(tbuffer[2] == data);
  TEST_ASSERT_EQUAL_UINT32(1, grb_getOne(uint8_t *, &ringBuffer, &data));
  TEST_ASSERT_TRUE(tbuffer[0] == data);
  TEST_ASSERT_EQUAL_UINT32(1, grb_getOne(uint8_t *, &ringBuffer, &data));
  TEST_ASSERT_TRUE(tbuffer[1] == data);
  TEST_ASSERT_EQUAL_UINT32(1, grb_getOne(uint8_t *, &ringBuffer, &data));
  TEST_ASSERT_TRUE(tbuffer[2] == data);
}


static void test_grb_putAll_overflow() {
  uint16_t buffer[2];
  size_t n = sizeof(buffer) / sizeof(buffer[0]);
  RingBuffer_t ringBuffer = grb_create(uint16_t, buffer, n);
  uint16_t tbuffer[] = { 1000, 8000, 16000 };
  size_t tn = sizeof(tbuffer) / sizeof(tbuffer[0]);

  TEST_ASSERT_EQUAL_UINT32(n, grb_putAll(uint16_t, &ringBuffer, tbuffer, tn));

  uint16_t data;

  TEST_ASSERT_EQUAL_UINT32(1, grb_getOne(uint16_t, &ringBuffer, &data));
  TEST_ASSERT_EQUAL_UINT16(tbuffer[0], data);
  TEST_ASSERT_EQUAL_UINT32(1, grb_getOne(uint16_t, &ringBuffer, &data));
  TEST_ASSERT_EQUAL_UINT16(tbuffer[1], data);
  /* Nothing left in the buffer */
  TEST_ASSERT_EQUAL_UINT32(0, grb_getOne(uint16_t, &ringBuffer, &data));
}


static void test_grb_getAll() {
  uint8_t *buffer[42];
  size_t n = sizeof(buffer) / sizeof(buffer[0]);
  RingBuffer_t ringBuffer = grb_create(uint8_t *, buffer, n);
  uint8_t *tbuffer[3];
  size_t tn = sizeof(tbuffer) / sizeof(tbuffer[0]);
  tbuffer[0] = "one";
  tbuffer[1] = "two";
  tbuffer[2] = "three";

  TEST_ASSERT_EQUAL_UINT32(tn, grb_putAll(uint8_t *, &ringBuffer, tbuffer, tn));

  uint8_t *data[42];

  TEST_ASSERT_EQUAL_UINT32(tn, grb_getAll(uint8_t *, &ringBuffer, data, sizeof(data) / sizeof(data[0])));

  for (uint8_t i = 0; i < tn; i++) {

    TEST_ASSERT_TRUE(tbuffer[i] == data[i]);

  }
}

/******************************************************************************/
/* END OF TEST GENERIC RING BUFFER ********************************************/
/******************************************************************************/
#endif /* not __RING_BUFFER_DONT_USE_GENERIC__ */

static void test_rb_putPointer_rb_getPointer() {
  uint8_t *ptr8 = (uint8_t *)0x8;
  uint16_t *ptr16 = (uint16_t *)0x16;
  uint32_t *ptr32 = (uint32_t *)0x32;
  RingBuffer_t *ptrRingBuffer = (RingBuffer_t *)0xBF;
  void *p;

  uint8_t buffer[42 * sizeof(void *)];
  size_t n = sizeof(buffer);
  RingBuffer_t ringBuffer = rb_create(buffer, n);

  rb_putPointer(&ringBuffer, ptr8);

  TEST_ASSERT_EQUAL_UINT32(1 * sizeof(void *), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  rb_putPointer(&ringBuffer, ptr16);

  TEST_ASSERT_EQUAL_UINT32(2 * sizeof(void *), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  rb_putPointer(&ringBuffer, ptr32);

  TEST_ASSERT_EQUAL_UINT32(3 * sizeof(void *), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  rb_putPointer(&ringBuffer, ptrRingBuffer);

  TEST_ASSERT_EQUAL_UINT32(4 * sizeof(void *), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  rb_getPointer(&ringBuffer, &p);

  TEST_ASSERT_EQUAL(ptr8, p);
  TEST_ASSERT_EQUAL_UINT32(3 * sizeof(void *), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  rb_getPointer(&ringBuffer, &p);

  TEST_ASSERT_EQUAL(ptr16, p);
  TEST_ASSERT_EQUAL_UINT32(2 * sizeof(void *), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  rb_getPointer(&ringBuffer, &p);

  TEST_ASSERT_EQUAL(ptr32, p);
  TEST_ASSERT_EQUAL_UINT32(1 * sizeof(void *), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  rb_getPointer(&ringBuffer, &p);

  TEST_ASSERT_EQUAL(ptrRingBuffer, p);
  TEST_ASSERT_EQUAL_UINT32(0 * sizeof(void *), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));
}


static void test_rb_getLastPointer() {
  uint8_t *ptr8 = (uint8_t *)0x8;
  uint16_t *ptr16 = (uint16_t *)0x16;
  void *p;

  uint8_t buffer[42 * sizeof(void *)];
  size_t n = sizeof(buffer);
  RingBuffer_t ringBuffer = rb_create(buffer, n);

  rb_putPointer(&ringBuffer, ptr8);

  TEST_ASSERT_EQUAL_UINT32(1 * sizeof(void *), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  rb_getLastPointer(&ringBuffer, &p);

  TEST_ASSERT_EQUAL(ptr8, p);
  /* rb_getLastPointer() is a "read-only" function, so nothing has to change
     inside ringBuffer data structure */
  TEST_ASSERT_EQUAL_UINT32(1 * sizeof(void *), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  rb_putPointer(&ringBuffer, ptr16);

  TEST_ASSERT_EQUAL_UINT32(2 * sizeof(void *), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  rb_getLastPointer(&ringBuffer, &p);

  TEST_ASSERT_EQUAL(ptr16, p);
  /* rb_getLastPointer() is a "read-only" function, so nothing has to change
     inside ringBuffer data structure */
  TEST_ASSERT_EQUAL_UINT32(2 * sizeof(void *), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  rb_getPointer(&ringBuffer, &p);

  TEST_ASSERT_EQUAL(ptr8, p);
  TEST_ASSERT_EQUAL_UINT32(1 * sizeof(void *), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));

  rb_getLastPointer(&ringBuffer, &p);

  TEST_ASSERT_EQUAL(ptr16, p);
  /* rb_getLastPointer() is a "read-only" function, so nothing has to change
     inside ringBuffer data structure */
  TEST_ASSERT_EQUAL_UINT32(1 * sizeof(void *), rb_getSize(&ringBuffer));
  TEST_ASSERT_EQUAL_UINT32(n, rb_getCapacity(&ringBuffer));
}


int main() {
  UNITY_BEGIN();
  RUN_TEST(test_rb_create);
  RUN_TEST(test_rb_putOne_rb_getOne);
  RUN_TEST(test_rb_putAll_normal);
  RUN_TEST(test_rb_putAll_overflow);
  RUN_TEST(test_rb_getAll);
  RUN_TEST(test_rb_testOneLast_normal);
  RUN_TEST(test_rb_testOneLast_beyond_limits);

  RUN_TEST(test_grb_create);
  RUN_TEST(test_grb_putOne_grb_getOne);
  RUN_TEST(test_grb_putAll_normal);
  RUN_TEST(test_grb_putAll_overflow);
  RUN_TEST(test_grb_getAll);

  RUN_TEST(test_rb_putPointer_rb_getPointer);
  RUN_TEST(test_rb_getLastPointer);

  return UNITY_END();
}
